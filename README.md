# Diskuv Assets

Welcome to the assets section of Diskuv. This is the home of releases and packages that can be downloaded without authentication.

The assets are *not* free for use. The assets are *not* free to copy. Unless otherwise stated, the assets are covered by the "DkSDK SOFTWARE DEVELOPMENT KIT LICENSE AGREEMENT" available at https://diskuv.com/legal/ to DkSDK subscribers.
